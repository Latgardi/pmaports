# Maintainer: Martijn Braam <martijn@brixit.nl>
pkgname=postmarketos-tweaks
pkgver=0.7.1
pkgrel=0
pkgdesc="Tweak tool for phone UIs"
url="https://gitlab.com/postmarketOS/postmarketos-tweaks"
arch="noarch"
license="GPL-3.0-or-later"
subpackages="$pkgname-phosh:phosh $pkgname-pinephone:pinephone"
depends="python3 py3-gobject3 py3-yaml gtk+3.0 libhandy1"
makedepends="py3-setuptools glib-dev libhandy1-dev meson"
install="$pkgname.post-install $pkgname.post-upgrade"
source="$pkgname-$pkgver.tar.gz::https://gitlab.com/postmarketOS/postmarketos-tweaks/-/archive/$pkgver/postmarketos-tweaks-$pkgver.tar.gz"
options="!check" # There's no testsuite

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"

	amove usr/share/postmarketos-tweaks/phosh.yml
}

pinephone() {
	install_if="$pkgname=$pkgver-r$pkgrel device-pine64-pinephone"

	amove usr/share/postmarketos-tweaks/pinephone.yml
}

sha512sums="
35c26d67f15f707cf196f198abcc396a2745358b0f3a108ae2d17aca5fc2518d82a65a60745f3cf40272fda9fbaabebb117697c91753b910ef4b48e785ff265f  postmarketos-tweaks-0.7.1.tar.gz
"
