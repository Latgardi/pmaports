# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=content-hub
pkgver=0_git20210515
_commit="0c1ba2d0a65813462408cda5293f2ee4b18ebbeb"
pkgrel=0
pkgdesc="Content sharing/picking infrastructure and service, designed to allow apps to securely and efficiently exchange content"
url="https://gitlab.com/ubports/core/content-hub"
arch="all"
license="GPL-3.0 LGPL-3.0"
depends_dev="qt5-qtdeclarative-dev lomiri-app-launch-dev gsettings-qt-dev libnih-dev lomiri-download-manager-dev libnotify-dev libapparmor-dev lomiri-ui-toolkit-dev"
makedepends="$depends_dev cmake cmake-extras libapparmor gtest-dev"
checkdepends="gmock dbus-test-runner xvfb-run"
options="!check" # 92% tests passed, 1 tests failed out of 12
source="https://gitlab.com/ubports/core/content-hub/-/archive/$_commit/content-hub-$_commit.tar.gz
	0001-Use-qmlplugindump-qt5.patch
	"
subpackages="$pkgname-dev $pkgname-lang"
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DWerror=OFF
	make -C build
}

check() {
	cd build/tests
	xvfb-run env CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
8e59db69af5983ee874747d287a84074b5b616051e611d56071691733766237dd53e4913caf3d150f94fd0c6ab3a5ee5c55b225d5253f22d8fe6ac10b7e97640  content-hub-0c1ba2d0a65813462408cda5293f2ee4b18ebbeb.tar.gz
2ee8dbc957b22c123c3116ab7540a28c01fcab2664a2fa9707aabdaeff952900e96f7b8121338acfea2c2bd5a0b806f61835e2bcd8e3f17ed757841587365b0f  0001-Use-qmlplugindump-qt5.patch
"
