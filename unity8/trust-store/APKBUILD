# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=trust-store
pkgver=0_git20210514
_commit="bc8c434686d79aaeeb94da63716439363528f76b"
pkgrel=0
pkgdesc="An API for creating, reading, updating and deleting trust requests answered by users."
url="https://github.com/ubports/trust-store"
arch="all"
license="LGPL-3.0-only"
makedepends="cmake cmake-extras mir-dev boost-dev process-cpp-dev dbus-cpp-dev properties-cpp-dev libapparmor-dev libapparmor qt5-qtdeclarative-dev gtest-dev gmock"
checkdepends="coreutils"
source="$pkgname-$_commit.tar.gz::https://github.com/ubports/$pkgname/archive/$_commit.tar.gz
	no_werror.patch"
subpackages="$pkgname-dev $pkgname-lang"
builddir="$srcdir/$pkgname-$_commit"
options="!check" # 62% tests passed, 5 tests failed out of 13

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make -C build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
db67fa953e54798faa5e749edfc027a87328b41f017b897ac99fee4fdfbc980124a39601017ead34a5536335811970757ebffcfaeae1d59e3f7227c2954965d7  trust-store-bc8c434686d79aaeeb94da63716439363528f76b.tar.gz
b839985eaedbab84240a619f5775b4b1d4a22cb38aec3314d125665550889cde845ba86f065346dc83aac8a03900feae62c69db69e4cb2983b9f20128c373ffb  no_werror.patch
"
